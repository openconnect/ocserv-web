---
title: Download ocserv's source code
layout: single
permalink: /download
author_profile: false
page_css:
 - /assets/css/expanded-single.css
sidebar:
  nav: "download"
---

<div class="side-by-side-container">
  <div class="side">
    <p>
	<h3><a class="plausible-event-name=Download" href="https://www.infradead.org/ocserv/download/">Download ocserv's sources</a></h3>
	<h3>Latest version is {% include version.inc %}</h3>
    </p>
  </div>
  <div class="side">
    <p>
	<a href="https://gitlab.com/openconnect/ocserv/">
	<h3>Follow development on gitlab</h3>
	<img src="https://about.gitlab.com/images/press/logo/png/gitlab-logo-gray-rgb.png" height="128px" alt="gitlab"/></a>
    </p>
  </div>
</div>

<div class="note">
<strong>Note:</strong> All releases are signed with the PGP key <a href="/assets/keys/96865171.asc">96865171</a>.
</div>
