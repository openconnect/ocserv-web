---
title: Getting help
layout: single
permalink: /help
author_profile: false
---

If you have problems building or using OpenConnect, or other questions
or comments, *please* use the IRC channel or send email to the mailing
list described below. You don't need to be subscribed to the list; you
only need to click on the email address below, and send a *plain text*
(not HTML) mail.

## IRC

There is an IRC channel
[#openconnect](ircs://irc.oftc.net:6697/#openconnect) on the [OFTC
network](https://www.oftc.net/). You can access it via the [OFTC
webchat](https://webchat.oftc.net/?channels=openconnect) if you don't
have an IRC client.

## Mailing list

There is a mailing list at
[openconnect-devel@lists.infradead.org](mailto:openconnect-devel@lists.infradead.org).
The list does not accept HTML email, so please make sure you post as
plain text only.

You do *not* have to be subscribed to the list in
order to post a question.

If you want to subscribe to the mailing list, you can do so from the
[Mailman admin
page](http://lists.infradead.org/mailman/listinfo/openconnect-devel).

## Reporting bugs

Issues should be reported to the mailing list or [the issue
tracker](https://gitlab.com/openconnect/ocserv/issues). Make sure you
provide, the version of ocserv used, the version of the GnuTLS library,
as well as all necessary steps to reproduce the issue and the relevant
snippets from logs. If it is a crash, please provide a backtrace.
