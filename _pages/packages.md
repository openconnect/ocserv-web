---
title: Ocserv packages are available on all major platforms
layout: single
permalink: /packages
page_css:
 - /assets/css/fancy-feature-row.css
 - /assets/css/expanded-single.css
sidebar:
  nav: "download"
feature_row:
  - title: "Ubuntu"
    excerpt: "The ocserv package is included in Ubuntu universe. Install with `apt-get install -y ocserv`"
  - title: "RHEL / CentOS"
    excerpt: "ocserv is available in the EPEL repository. Install after enabling EPEL with `yum install -y ocserv`."
  - title: "Fedora"
    excerpt: "ocserv is available in Fedora. Install with `yum install -y ocserv`."
  - title: "Debian"
    excerpt: "ocserv is available in Debian. Install with `apt-get install -y ocserv`."
  - title: "FreeBSD"
    excerpt: "There are [ports packages](https://www.freshports.org/net/ocserv/) available."
  - title: "OpenBSD"
    excerpt: "There are [ports packages](http://ports.su/net/ocserv) available."
  - title: "OpenWRT"
    excerpt: "ocserv is part of [OpenWRT packages collection](https://github.com/openwrt/packages/tree/master/net/ocserv/)."
  - title: "VyOS"
    excerpt: "ocserv is [available in VyOS](https://docs.vyos.io/en/equuleus/configuration/vpn/openconnect.html)."
---

<div class="container mt-5">
    <div class="mb-4" style="padding-top: 2em;">
{% include feature_row %}
    </div>
</div>
