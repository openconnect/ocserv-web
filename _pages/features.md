---
layout: splash
permalink: /features
page_css:
 - /assets/css/fancy-feature-row.css
feature_row:
  - title: "Authentication"
    excerpt: "First and second factor authentication. Authentication using PAM, Radius, password file, one time passwords (HOTP/TOTP), OpenID Connect, smart card, certificate authentication, and Kerberos with GSSAPI/SPNEGO."
  - title: "Accounting"
    excerpt: "Get client usage statistics via API or via the Radius accounting protocol."
  - title: "Security"
    excerpt: "Depend standard protocols only: TLS and Datagram TLS. The server key is protected by a software security module, and can further be protected by TPM, or by a hardware security module (HSM)."
  - title: "Networking"
    excerpt: "Support for IPv6 and IPv4 and collocation (port sharing) with an HTTPS server. Operate behind a proxy using the Proxy Protocol. Routes can be pushed from server to client and vice versa."
  - title: "Scalability"
    excerpt: "The number of connected clients and processing ability scales with the number of CPUs."
  - title: "Control interface"
    excerpt: "Query for statistics and issue commands to the server. The occtl tool works interactively or as an API with JSON output."
  - title: "Reliability"
    excerpt: "Two concurrent VPN channels; the primary is over UDP/DTLS for performance, and the control+backup is over over TCP/TLS."
  - title: "Resource limits"
    excerpt: "Set resource limits per client or groups of clients, in bandwidth, network priority, as well as confining clients in specific cgroups."
  - title: "Client Isolation"
    excerpt: "Each client is isolated on a separate isolated (seccomp) process with a separate networking device and IP. The server has modular design using privilege separation from the client worker processes."
---

# Ocserv features

{% include feature_row %}
