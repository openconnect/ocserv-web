---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/jordan-harrison-40XgDxBfYXM-unsplash-1.jpg
  overlay_filter: 0.6
  actions:
    - label: "<i class='fas'></i> Deploy on your platform"
      url: "/packages"
    - label: "<i class='fas fa-download'></i> Download"
      url: "/download"
excerpt: >
  OpenConnect VPN server (ocserv) is an open source Linux SSL VPN server designed for organizations that require a remote access VPN with enterprise user management and control.
feature_row:
  - title: "Enterprise grade security"
    excerpt: "OpenConnect VPN server is designed for privacy, protecting the clients from accessing each others data using strict isolation and privilege separation. It secures the VPN channels using only standard protocols like TLS and Datagram TLS and prevents the leakage of cryptographic keys with Hardware Security Modules (HSMs).<br/>"
    url: "/features/"
    btn_class: "btn--primary"
    btn_label: "Learn more"
  - title: "Scalability"
    excerpt: "Ocserv is design to take advantage of the immense processing power available on public clouds and the features of low-power CPUs. Its performance serving clients scales linearly with number of CPUs and its compact memory footprint ensures that cheaper hosts can provide the necessary resources to handle thousands of clients."
    url: "/technical"
    btn_class: "btn--primary"
    btn_label: "Learn more"
  - title: "Enterprise authentication"
    excerpt: "The server's authentication module can operate using a simple password file, to authentication methods that integrate with your organization's setup, such as Radius, OpenID Connect, Kerberos, smart cards and combinations of them to enable two-factor authentication. It further enables you to obtain detailed accounting reports using Radius.<br/>"
    url: "/ocserv.8.html"
    btn_class: "btn--primary"
    btn_label: "Learn more"      
  - title: "Deploy ocserv"
    excerpt: "There are various deployment scenarios of ocserv ranging from letsencrypt integration to FreeIPA and Radius integration. Learn how to deploy ocserv at our how-to guide.<br/><br/>"
    url: "https://docs.openconnect-vpn.net/recipes/"
    btn_class: "btn--primary"
    btn_label: "Read the how-to"
  - title: "Manage ocserv"
    excerpt: "Openconnect server provides a user management interface that allows you to query and monitor the server and its users. Access it interactive or get output in JSON format."
    url: "/occtl.8.html"
    btn_class: "btn--primary"
    btn_label: "Read the docs"
  - title: "Getting help"
    excerpt: "OpenConnect is a community project; get to know the community and ask for help.<br/><br/><br/><br/>"
    url: "/help/"
    btn_class: "btn--primary"
    btn_label: "Learn more"
---

{% include feature_row %}
