# ocserv web pages
This repository contains the source code for the web pages at:
 * https://ocserv.openconnect-vpn.net

## Setup

To apply changes, fork this repository (or create a branch).
After forking the repo and updating the pages run:

- `make`
- `bundle exec jekyll serve`

## Environment

The web site is based [on Minimal-Mistakes](https://mmistakes.github.io/minimal-mistakes/)
Jekyll theme.
