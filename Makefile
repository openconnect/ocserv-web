#

all: _pages/ocserv.8.md _pages/occtl.8.md _pages/changelog.md

clean:
	rm -f _pages/ocserv.8.md _pages/occtl.8.md _pages/changelog.md

.PHONY: _includes/version.inc _pages/occtl.8.md _pages/ocserv.8.md _pages/changelog.md

BRANCH=master

ocserv-git.stamp:
	rm -rf ocserv-git
	git clone --depth 1 https://gitlab.com/openconnect/ocserv.git -b $(BRANCH) ocserv-git
	touch $@

_pages/changelog.md: ocserv-git.stamp
	sed -e 's|@TITLE@|Changelog|g' -e 's|@PERMALINK@|/changelog|g' tmpl/changelog-header.tmpl > $@
	cat ./ocserv-git/NEWS|sed 's|\#\([0-9]\+\)|https://gitlab.com/openconnect/ocserv/issues/\1[\#\1]|g'|asciidoc -s -b xhtml11 - >> $@

_includes/version.inc: ocserv-git.stamp
	cat ./ocserv-git/NEWS|grep "released 20"|head -1|cut -d ' ' -f 3 > $@

_pages/ocserv.8.md: ocserv-git.stamp ./ocserv-git/doc/ocserv.8.md _includes/version.inc
	sed -e 's/^/    /' ocserv-git/doc/sample.config >>sample.config.tmp
	sed -e '/@CONFIGFILE@/{r sample.config.tmp' -e 'd}' <./ocserv-git/doc/ocserv.8.md >ocserv.tmp
	sed -e 's|@TITLE@|Manual|g' -e 's|@PERMALINK@|/ocserv.8.html|g' tmpl/docs-header.tmpl > $@
	cat ocserv.tmp >> $@
	rm -f sample.config.tmp

_pages/occtl.8.md: ocserv-git.stamp ./ocserv-git/doc/occtl.8.md _includes/version.inc
	sed -e 's|@TITLE@|Occtl|g' -e 's|@PERMALINK@|/occtl.8.html|g' tmpl/docs-header.tmpl > $@
	cat ./ocserv-git/doc/occtl.8.md >> $@

